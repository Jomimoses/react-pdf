import React, { Component } from "react";
import image from "../assets/images/test.jpg";

class LetterPad extends Component {
  renderParagraph = () => (
    <>
      <h3>Creating extra spaces before or after text</h3>
      <p>
        &nbsp;&nbsp; Any HTML element can have additional spacing added to the
        top, right, bottom, or left. However, make sure you understand the
        difference between margin and padding before deciding the type of space
        you want to add around the element or object. As you can see in the
        picture below, padding is what immediately surrounds the element, within
        the border, and the margin outside the border.
      </p>
    </>
  );

  renderGridView = () => {
    return (
      <>
        <h3>Image grid view</h3>
        <div className="wrap-view">
          <img className="img-small" src={image} alt="Nature" />
          <img className="img-small" src={image} alt="Nature" />
          <img className="img-small" src={image} alt="Nature" />
          <img className="img-small" src={image} alt="Nature" />
        </div>
        <p style={{ textAlign: "center" }}>
          <small>grid images</small>
        </p>
      </>
    );
  };

  checBox = () => (
    <>
      <br />
      <div class="form-check">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" name="remember" />{" "}
          Remember me
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input type="checkbox" class="form-check-input" value="" />
          Option 1
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input type="checkbox" class="form-check-input" value="" />
          Option 2
        </label>
      </div>
    </>
  );

  radioButton = () => (
    <>
      <br />

      <div class="form-check-inline">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optradio" />
          Option 1
        </label>
      </div>
      <div class="form-check-inline">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optradio" />
          Option 2
        </label>
      </div>
    </>
  );

  selectView = () => (
    <>
      <br />
      <div class="form-group">
        <label for="sel1">Select list:</label>
        <select class="form-control" id="sel1">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </select>
      </div>
    </>
  );

  renderForms = () => {
    return (
      <div>
        <h3>Form View</h3>
        <br />
        <form class="form" action="/action_page.php">
          <label for="email">Email:</label>
          <input
            type="email"
            class="form-control"
            id="email"
            placeholder="Enter email"
            name="email"
          />
          <label for="pwd">Password:</label>
          <input
            type="password"
            class="form-control"
            id="pwd"
            placeholder="Enter password"
            name="pswd"
          />
          {this.checBox()}
          {this.radioButton()}
          {this.selectView()}
          {this.renderParagraph()}

          <div className="img-view">
            <img
              className="img"
              src={image}
              alt="Nature"
            />
            <p>
              <small>nature image</small>
            </p>
          </div>
        </form>
      </div>
    );
  };

  render() {
    return (
      <div className="bg-gray">
        <div id="pdf" className="a4">
          {this.renderParagraph()}

          <h1>Heading 1</h1>
          <h2>Heading 2</h2>
          <h3>Heading 3</h3>
          <h4>Heading 4</h4>
          <h5>Heading 5</h5>
          <h6>Heading 6</h6>
          {this.renderGridView()}
        </div>
        <div id="pdf1" className="a4">
          {this.renderForms()}
        </div>
      </div>
    );
  }
}
export default LetterPad;
