import React, { Component, Fragment } from "react";
import { html2Pdf } from "../helpers/helpers";
import LetterPad from "./letterPad";

class Dashboard extends Component {
  renderLetter = () => {
    return (
      <div>
        <div className="btn-view">
          <button
            className="btn btn-primary"
            onClick={() => html2Pdf(["pdf", "pdf1"])}
          >
            Download
          </button>
        </div>
        <LetterPad />
      </div>
    );
  };

  render() {
    return <Fragment>{this.renderLetter()}</Fragment>;
  }
}
export default Dashboard;
