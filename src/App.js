import React, { Fragment } from "react";
import Dashboard from "./components/dashboard";

function App() {
  return (
    <Fragment>
      <Dashboard />
    </Fragment>
  );
}

export default App;
